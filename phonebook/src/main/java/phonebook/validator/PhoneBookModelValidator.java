package phonebook.validator;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import phonebook.model.PhoneBookModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class PhoneBookModelValidator implements Validator {
    private static ThreadLocal<DateFormat> dateFormat = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy.MM.dd");
        }
    };

    private static String lastnameValidSize = "lastname valid size=[0,19]";
    private static String firstnameValidSize = "firstname valid size=[0,9]";
    private static String mobilephoneValidMask = "mobilephone valid mask ddd-ddd-dd-dd";
    private static String workphoneValidMask = "workphone valid mask ddd-ddd-dd-dd";
    private static String workphoneMustMatch = "workphone must match the first three digits with mobilephone";
    private static String mobilephoneMustMatch = "mobilephone must match the first three digits with workphone";
    private static String emailValidMask = "email valid mask s{1,30}@s{1,10}";
    private static String birthdayValidMask = "birthdate valid mask yyyy.mm.dd";
    private static String birthdateYearValid = "birthdate year must be [1900,current year]";
    private static Pattern phonePattern = Pattern.compile("^\\d\\d\\d-\\d\\d\\d-\\d\\d-\\d\\d$");
    private static Pattern emailPattern = Pattern.compile("^[A-Za-z0-9._-]{1,30}@[A-Za-z0-9._-]{1,10}$");
    private static Pattern birthdatePattern = Pattern.compile("^\\d\\d\\d\\d.\\d\\d.\\d\\d$");

    @Override
    public boolean supports(Class<?> aClass) {
        return PhoneBookModel.class.equals(aClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        PhoneBookModel phoneBookModel = (PhoneBookModel) obj;
        if (phoneBookModel.getLastname().length() > 19) {
            errors.rejectValue("lastname", lastnameValidSize, lastnameValidSize);
        }
        if (phoneBookModel.getFirstname().length() > 9) {
            errors.rejectValue("firstname", firstnameValidSize, firstnameValidSize);
        }

        Matcher mobilephoneMatcher = phonePattern.matcher(phoneBookModel.getMobilephone());
        if (!mobilephoneMatcher.matches()) {
            errors.rejectValue("mobilephone", mobilephoneValidMask, mobilephoneValidMask);
        }
        Matcher workphoneMatcher = phonePattern.matcher(phoneBookModel.getWorkphone());
        if (!workphoneMatcher.matches()) {
            errors.rejectValue("workphone", workphoneValidMask, workphoneValidMask);
        }

        if (errors.getFieldError("mobilephone") == null && errors.getFieldError("workphone") == null) {
            if (!phoneBookModel.getMobilephone().substring(0, 3).equals(phoneBookModel.getWorkphone().substring(0, 3))) {
                errors.rejectValue("workphone", workphoneMustMatch, workphoneMustMatch);
                errors.rejectValue("mobilephone", mobilephoneMustMatch, mobilephoneMustMatch);
            }
        }

        Matcher emailMatcher = emailPattern.matcher(phoneBookModel.getMail());
        if (!emailMatcher.matches()) {
            errors.rejectValue("mail", emailValidMask, emailValidMask);
        }

        Matcher birthdateMatcher = birthdatePattern.matcher(phoneBookModel.getBirthdate());
        if (!birthdateMatcher.matches()) {
            errors.rejectValue("birthdate", birthdayValidMask, birthdayValidMask);
        } else {
            Date birthDate = null;
            try {
                birthDate = dateFormat.get().parse(phoneBookModel.getBirthdate());
            } catch (ParseException e) {
                errors.rejectValue("birthdate", birthdayValidMask, birthdayValidMask);
            }
            if (birthDate != null) {
                Calendar calendarBirthDate = new GregorianCalendar();
                calendarBirthDate.setTime(birthDate);
                int year = calendarBirthDate.get(Calendar.YEAR);

                Calendar calendarCurrentDate = new GregorianCalendar();
                calendarCurrentDate.setTime(new Date());
                int currentYear = calendarCurrentDate.get(Calendar.YEAR);

                if (year < 1900 || year > currentYear) {
                    errors.rejectValue("birthdate", birthdateYearValid, birthdateYearValid);
                }
            }
        }
    }
}