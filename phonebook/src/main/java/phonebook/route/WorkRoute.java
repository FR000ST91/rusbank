package phonebook.route;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import core.entity.PhoneBookEntity;
import core.entity.PlaceOfWorkEntity;
import core.repository.PhoneBookRepository;
import core.repository.PlaceOfWorkRepository;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.GenericFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import phonebook.pojo.PhoneBookPojo;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Component
public class WorkRoute extends RouteBuilder {
    @Value("${workFolder}")
    private String workFolder;
    @Value("${dataFolder}")
    private String dataFolder;
    @Value("${errorFolder}")
    private String errorFolder;
    private String schema;
    private Gson gson;
    @Autowired
    private PlaceOfWorkRepository placeOfWorkRepository;
    @Autowired
    private PhoneBookRepository phoneBookRepository;

    @PostConstruct
    private void postConstruct() throws IOException {
        ClassPathResource classPathResource = new ClassPathResource("phone_book_verification.json");
        InputStream inputStream = classPathResource.getInputStream();
        File tempVerificationJson = File.createTempFile("phone_book_verification", ".json");
        try {
            FileUtils.copyInputStreamToFile(inputStream, tempVerificationJson);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

        schema = FileUtils.readFileToString(tempVerificationJson, StandardCharsets.UTF_8);
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @Override
    public void configure() throws Exception {
        from("file:" + workFolder + "?delay=5000&include=.*\\.txt&doneFileName=${file:name.noext}.ready")
                .routeId("route-1")
                .log("Взяли в работу файл ${body.absoluteFilePath}")
                .onException(Exception.class)
                .handled(true)
                .log("Файл ${body.absoluteFilePath} не прошёл валидацию или произошла другая ошибка. Файл перемещён в папку " + errorFolder)
                .to("file:" + errorFolder)
                .end()
                .process(exchange -> {
                    String json = FileUtils.readFileToString((File) ((GenericFile) exchange.getIn().getBody()).getFile(), Charset.defaultCharset());
                    validate(schema, json);
                    exchange.getIn().setHeader("json", json);
                })
                .log("Файл ${body.absoluteFilePath} прошёл валидацию")
                .to("file:" + dataFolder)
                .log("Файл ${body.absoluteFilePath} записан в папку data")
                .process(exchange -> {
                    String json = (String) exchange.getIn().getHeader("json");
                    PhoneBookPojo phoneBookPojo = gson.fromJson(json, PhoneBookPojo.class);
                    PlaceOfWorkEntity placeOfWorkEntity = placeOfWorkRepository.findByLastnameAndFirstname(phoneBookPojo.getLastname(), phoneBookPojo.getFirstname());
                    PhoneBookEntity phoneBookEntity = new PhoneBookEntity();
                    phoneBookEntity.setLastname(phoneBookPojo.getLastname());
                    phoneBookEntity.setFirstname(phoneBookPojo.getFirstname());
                    phoneBookEntity.setMobilephone(phoneBookPojo.getMobilephone());
                    phoneBookEntity.setWorkphone(phoneBookPojo.getWorkphone());
                    phoneBookEntity.setMail(phoneBookPojo.getMail());
                    phoneBookEntity.setBirthdate(phoneBookPojo.getBirthdate());
                    if (placeOfWorkEntity != null) {
                        String work = placeOfWorkEntity.getWork() + " " + placeOfWorkEntity.getWorkaddress();
                        phoneBookEntity.setWork(work);
                        phoneBookPojo.setWork(work);
                        String phoneBookPojoJson = gson.toJson(phoneBookPojo);
                        FileUtils.writeStringToFile(new File((String) exchange.getIn().getHeader("CamelFileNameProduced")), phoneBookPojoJson, Charset.defaultCharset());
                    }
                    phoneBookRepository.save(phoneBookEntity);
                })
                .log("Распарсили файл ${body.absoluteFilePath}, обогатили и разложили в БД");
    }

    public void validate(String validationSchema, String subject) {
        JSONObject jsonSchema = new JSONObject(new JSONTokener(IOUtils.toInputStream(validationSchema, Charset.defaultCharset())));
        JSONObject jsonSubject = new JSONObject(new JSONTokener(IOUtils.toInputStream(subject, Charset.defaultCharset())));
        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
    }
}