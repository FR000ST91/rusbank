package phonebook.model;

import phonebook.pojo.PhoneBookPojo;

import java.util.LinkedHashMap;
import java.util.Map;

public class PhoneBookModel {
    private String lastname;
    private String firstname;
    private String workphone;
    private String mobilephone;
    private String mail;
    private String birthdate;
    private String fileName;
    private String status;
    private Map<String, Boolean> disabledElements;

    public PhoneBookModel() {
        disabledElements = new LinkedHashMap<>();
        disabledElements.put("clean", false);
        disabledElements.put("add", false);
        disabledElements.put("send", true);
        disabledElements.put("input", false);
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getWorkphone() {
        return workphone;
    }

    public void setWorkphone(String workphone) {
        this.workphone = workphone;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, Boolean> getDisabledElements() {
        return disabledElements;
    }

    public void setDisabledElements(Map<String, Boolean> disabledElements) {
        this.disabledElements = disabledElements;
    }

    public void checkUnlock() {
        this.setFirstname(null);
        this.setLastname(null);
        this.setFileName(null);
        this.setBirthdate(null);
        this.setMail(null);
        this.setMobilephone(null);
        this.setWorkphone(null);
        this.getDisabledElements().put("input", false);
        this.getDisabledElements().put("add", false);
        this.getDisabledElements().put("send", true);
    }

    public void checkLock() {
        this.getDisabledElements().put("input", true);
        this.getDisabledElements().put("add", true);
        this.getDisabledElements().put("send", false);
    }

    public PhoneBookPojo createPhoneBookPojo() {
        PhoneBookPojo phoneBookPojo = new PhoneBookPojo();
        phoneBookPojo.setLastname(this.getLastname());
        phoneBookPojo.setFirstname(this.getFirstname());
        phoneBookPojo.setMobilephone(this.getMobilephone());
        phoneBookPojo.setWorkphone(this.getWorkphone());
        phoneBookPojo.setMail(this.getMail());
        phoneBookPojo.setBirthdate(this.getBirthdate());
        return phoneBookPojo;
    }
}