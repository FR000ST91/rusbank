package phonebook.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import phonebook.model.PhoneBookModel;
import phonebook.pojo.PhoneBookPojo;
import phonebook.validator.PhoneBookModelValidator;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@SessionAttributes(value = "phoneBookModel")
public class PhoneBookController {
    private static ThreadLocal<DateFormat> dateFormat = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("ddMMyyyy_HHmm");
        }
    };

    @Value("${prepareFolder}")
    private String prepareFolder;
    @Value("${workFolder}")
    private String workFolder;
    @Value("${dataFolder}")
    private String dataFolder;
    private Gson gson;
    @Autowired
    private PhoneBookModelValidator phoneBookModelValidator;

    @InitBinder("phoneBookModel")
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(phoneBookModelValidator);
    }

    @PostConstruct
    private void postConstruct() {
        gson = new GsonBuilder().setPrettyPrinting().create();
    }


    @GetMapping("/phonebook")
    public String phoneBookForm(Model model) {
        model.addAttribute("phoneBookModel", new PhoneBookModel());
        return "phonebook";
    }

    @PostMapping(value = "/phonebook", params = "cleanPhoneBookForm")
    public String cleanPhoneBookFormPost(@Valid @ModelAttribute("phoneBookModel") PhoneBookModel phoneBookModel) {
        String status = null;
        if (phoneBookModel.getFileName() != null) {
            try {
                FileUtils.forceDelete(new File(phoneBookModel.getFileName()));
                status = "Файл " + phoneBookModel.getFileName() + " успешно удалён";
            } catch (IOException e) {
                status = "При удалении файла " + phoneBookModel.getFileName() + " возникла ошибка";
            }
        } else {
            status = "Файл с текущей формой ещё не создан";
        }
        phoneBookModel.checkUnlock();
        phoneBookModel.setStatus(status);
        return "phonebook";
    }

    @PostMapping(value = "/phonebook", params = "addPhoneBookForm")
    public String addPhoneBookFormPost(@Valid @ModelAttribute("phoneBookModel") PhoneBookModel phoneBookModel, BindingResult bindingResult) throws IOException {
        if (bindingResult.hasErrors()) {
            return "phonebook";
        }

        PhoneBookPojo phoneBookPojo = phoneBookModel.createPhoneBookPojo();
        String phoneBookJson = gson.toJson(phoneBookPojo);

        Date nowDate = new Date();
        String fileName = "data_" + dateFormat.get().format(nowDate) + ".json";

        FileUtils.writeStringToFile(new File(prepareFolder + fileName), phoneBookJson, Charset.defaultCharset());
        phoneBookModel.checkLock();
        phoneBookModel.setFileName(prepareFolder + fileName);
        phoneBookModel.setStatus("Файл " + phoneBookModel.getFileName() + " успешно создан");
        return "phonebook";
    }

    @PostMapping(value = "/phonebook", params = "sendPhoneBookForm")
    public String sendPhoneBookFormPost(@Valid @ModelAttribute("phoneBookModel") PhoneBookModel phoneBookModel) {
        String formatNowDate = dateFormat.get().format(new Date());
        String renameFileName = workFolder + "json_" + formatNowDate + ".txt";
        String fileFlagName = workFolder + "json_" + formatNowDate + ".ready";
        String status = null;
        try {
            FileUtils.moveFile(FileUtils.getFile(phoneBookModel.getFileName()), FileUtils.getFile(renameFileName));
        } catch (IOException e) {
            status = "Ошибка при перемещении файла " + phoneBookModel.getFileName() + " в " + renameFileName;
        }
        try {
            if (status == null) {
                FileUtils.touch(new File(fileFlagName));
            }
        } catch (IOException e) {
            status = "Ошибка при создании файла флага " + fileFlagName;
        }

        if (status == null) {
            status = "Файл " + phoneBookModel.getFileName() + " успешно перемещён в " + renameFileName;
        }
        phoneBookModel.checkUnlock();
        phoneBookModel.setStatus(status);
        return "phonebook";
    }
}
