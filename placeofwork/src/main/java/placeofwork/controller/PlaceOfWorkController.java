package placeofwork.controller;

import core.entity.PlaceOfWorkEntity;
import core.repository.PlaceOfWorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import placeofwork.model.PlaceOfWorkModel;
import placeofwork.model.SearchPlaceOfWorkModel;
import placeofwork.validator.PlaceOfWorkValidator;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
public class PlaceOfWorkController {
    @Autowired
    private PlaceOfWorkRepository placeOfWorkRepository;

    @Autowired
    private PlaceOfWorkValidator placeOfWorkValidator;

    private Map<Integer, String> allFilters;

    @PostConstruct
    private void postConstruct() {
        allFilters = new LinkedHashMap<>();
        allFilters.put(0, "Выберите поле");
        allFilters.put(1, "Ид(id)");
        allFilters.put(2, "Фамилия(lastname)");
        allFilters.put(3, "Имя(firstname)");
        allFilters.put(4, "Место работы(work)");
        allFilters.put(5, "Адрес работы(workaddress)");
    }

    @InitBinder("placeOfWorkModel")
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(placeOfWorkValidator);
    }

    @GetMapping("/addplaceofwork")
    public String addPlaceOfWork(Model model) {
        model.addAttribute("placeOfWorkModel", new PlaceOfWorkModel());
        return "addplaceofwork";
    }

    @PostMapping(value = "/addplaceofwork", params = "addPlaceOfWorkForm")
    public String addPlaceOfWorkFormPost(Model model, @Valid @ModelAttribute("placeOfWorkModel") PlaceOfWorkModel placeOfWorkModel, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addplaceofwork";
        }

        PlaceOfWorkEntity placeOfWorkEntity = new PlaceOfWorkEntity();
        placeOfWorkEntity.setLastname(placeOfWorkModel.getLastname());
        placeOfWorkEntity.setFirstname(placeOfWorkModel.getFirstname());
        placeOfWorkEntity.setWork(placeOfWorkModel.getWork());
        placeOfWorkEntity.setWorkaddress(placeOfWorkModel.getWorkaddress());

        try {
            PlaceOfWorkEntity save = placeOfWorkRepository.save(placeOfWorkEntity);
            model.addAttribute("status", "Место работы добавлено. Ид(id)=" + save.getId());
            placeOfWorkModel.clean();
        } catch (DataIntegrityViolationException e) {
            model.addAttribute("status", "Ошибка добавления места работы, у " + placeOfWorkEntity.getLastname() + " " + placeOfWorkEntity.getFirstname() + " уже есть работа");
        }

        return "addplaceofwork";
    }

    @PostMapping(value = "/addplaceofwork", params = "cleanPlaceOfWorkForm")
    public String cleanPlaceOfWorkFormPost(Model model, @ModelAttribute("placeOfWorkModel") PlaceOfWorkModel placeOfWorkModel) {
        model.addAttribute("status", "Форма очищена");
        placeOfWorkModel.clean();
        return "addplaceofwork";
    }

    @GetMapping("/listplaceofworks")
    public String showPlaceOfWorks(Model model) {
        model.addAttribute("searchPlaceOfWorkModel", new SearchPlaceOfWorkModel());
        model.addAttribute("allFilters", allFilters);
        model.addAttribute("placeofworks", entityToModel(placeOfWorkRepository.findAll()));
        return "listplaceofworks";
    }

    @PostMapping(value = "/listplaceofworks", params = "search")
    public String searchPlaceOfWorkFormPost(Model model, @ModelAttribute("searchPlaceOfWorkModel") SearchPlaceOfWorkModel searchPlaceOfWorkModel) {
        model.addAttribute("allFilters", allFilters);
        List<PlaceOfWorkModel> placeOfWorks = new ArrayList<>();
        switch (searchPlaceOfWorkModel.getFilterNumber()) {
            case 0: {
                placeOfWorks = entityToModel(placeOfWorkRepository.findAll());
                break;
            }
            case 1: {
                Integer id = null;
                try {
                    id = Integer.valueOf(searchPlaceOfWorkModel.getValue());
                } catch (NumberFormatException ignored) {
                }
                if (id != null) {
                    List<Integer> listIds = new ArrayList<>();
                    listIds.add(id);
                    placeOfWorks = entityToModel(placeOfWorkRepository.findAllById(listIds));
                } else {
                    placeOfWorks = new ArrayList<>();
                }
                break;
            }
            case 2: {
                placeOfWorks = entityToModel(placeOfWorkRepository.findByLastname(searchPlaceOfWorkModel.getValue()));
                break;
            }
            case 3: {
                placeOfWorks = entityToModel(placeOfWorkRepository.findByFirstname(searchPlaceOfWorkModel.getValue()));
                break;
            }
            case 4: {
                placeOfWorks = entityToModel(placeOfWorkRepository.findByWork(searchPlaceOfWorkModel.getValue()));
                break;
            }
            case 5: {
                placeOfWorks = entityToModel(placeOfWorkRepository.findByWorkaddress(searchPlaceOfWorkModel.getValue()));
                break;
            }
        }
        model.addAttribute("placeofworks", placeOfWorks);
        model.addAttribute("status", "Поиск выполнен успешно");
        return "listplaceofworks";
    }

    private List<PlaceOfWorkModel> entityToModel(Iterable<PlaceOfWorkEntity> placeOfWorks) {
        List<PlaceOfWorkModel> result = new ArrayList<>();
        for (PlaceOfWorkEntity current : placeOfWorks) {
            PlaceOfWorkModel placeOfWorkModel = new PlaceOfWorkModel();
            placeOfWorkModel.setId(current.getId());
            placeOfWorkModel.setFirstname(current.getFirstname());
            placeOfWorkModel.setLastname(current.getLastname());
            placeOfWorkModel.setWork(current.getWork());
            placeOfWorkModel.setWorkaddress(current.getWorkaddress());
            result.add(placeOfWorkModel);
        }
        return result;
    }
}
