package placeofwork.validator;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import placeofwork.model.PlaceOfWorkModel;

@Service
public class PlaceOfWorkValidator implements Validator {
    private static String lastnameValidSize = "lastname valid size=[0,128]";
    private static String firstnameValidSize = "firstname valid size=[0,128]";
    private static String workValidSize = "work valid size=[0,128]";
    private static String workaddressValidSize = "workaddress valid size=[0,128]";


    @Override
    public boolean supports(Class<?> aClass) {
        return PlaceOfWorkModel.class.equals(aClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        PlaceOfWorkModel placeOfWorkModel = (PlaceOfWorkModel) obj;
        if (placeOfWorkModel.getLastname().length() > 128) {
            errors.rejectValue("lastname", lastnameValidSize, lastnameValidSize);
        }
        if (placeOfWorkModel.getFirstname().length() > 128) {
            errors.rejectValue("firstname", firstnameValidSize, firstnameValidSize);
        }
        if (placeOfWorkModel.getWork().length() > 128) {
            errors.rejectValue("work", workValidSize, workValidSize);
        }
        if (placeOfWorkModel.getWorkaddress().length() > 128) {
            errors.rejectValue("workaddress", workaddressValidSize, workaddressValidSize);
        }
    }
}