package placeofwork.model;

public class SearchPlaceOfWorkModel {
    private Integer filterNumber;
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getFilterNumber() {
        return filterNumber;
    }

    public void setFilterNumber(Integer filterNumber) {
        this.filterNumber = filterNumber;
    }
}
