package core.repository;

import core.entity.PlaceOfWorkEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlaceOfWorkRepository extends CrudRepository<PlaceOfWorkEntity, Integer> {
    List<PlaceOfWorkEntity> findByLastname(String lastname);

    List<PlaceOfWorkEntity> findByFirstname(String firstname);

    List<PlaceOfWorkEntity> findByWork(String work);

    List<PlaceOfWorkEntity> findByWorkaddress(String workaddress);

    PlaceOfWorkEntity findByLastnameAndFirstname(String lastname, String firstname);
}