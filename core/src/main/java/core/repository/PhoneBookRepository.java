package core.repository;

import core.entity.PhoneBookEntity;
import org.springframework.data.repository.CrudRepository;

public interface PhoneBookRepository extends CrudRepository<PhoneBookEntity, Integer> {

}
