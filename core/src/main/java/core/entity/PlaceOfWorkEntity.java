package core.entity;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@DynamicInsert
@DynamicUpdate
@BatchSize(size = 1000)
@Table(name = "place_of_work", uniqueConstraints = {@UniqueConstraint(name = "firstname_lastname_constraint", columnNames = {"firstname", "lastname"})}) //constraint нужен для гаранта получения одной записи при обогащении
public class PlaceOfWorkEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Integer id;
    @Column(length = 128)
    private String firstname;
    @Column(length = 128)
    private String lastname;
    @Column(length = 128)
    private String work;
    @Column(length = 128)
    private String workaddress;

    public PlaceOfWorkEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getWorkaddress() {
        return workaddress;
    }

    public void setWorkaddress(String workaddress) {
        this.workaddress = workaddress;
    }
}
